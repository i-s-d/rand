; syscall.asm

; Вызываем системный вызов
; Принимает до пяти аргументов (не считая номера вызова)
global _nsyscall
_nsyscall:
  ; Перетасовываем регистры
  mov rax, rdi
  mov rdi, rsi
  mov rsi, rdx
  mov rdx, rcx
  mov r10, r8
  mov r8, r9
  ; Вызываем
  syscall
  ret

; Вызываем вызов с шестью аргументами
; На стеке при вызове находится:
; rsp: куда возвращаться <-- верх стека
; rsp+8: седьмой аргумент (шестой для вызова)
global _syscall6
_syscall6:
  ; То же самое...
  mov rax, rdi
  mov rdi, rsi
  mov rsi, rdx
  mov rdx, rcx
  mov r10, r8
  mov r8, r9
  mov r9, [rsp+8] ; Добываем шестой со стека
  ; Вызываем
  syscall
  ret
