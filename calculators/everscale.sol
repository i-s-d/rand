// SPDX-License-Identifier: WTFPL
/**

                  Everscale calculator
                    (c) i-s-d, 2023

    If I can easily write calculator in that language, it is
    pretty usable.
    Solidity from Everscale misses pointers, but calculator
    can be written.

  USAGE

    This thing has only two public things except
    contructor.
    The one named "calcStuff()" calculates
    expression you pass to it (for example "2 + 2").
    For now it supports integer numbers, usual four
    operators, parenthesis and single-argument functions.
    The other "vector(...) history" is just a history of ops.

  HACKING

    The calculator is based on the shunting yard algorithm,
    without nonstandard modifications. All three steps
    (tokenization, RPN generation and executing it) are
    merged into one loop.
    The operators are in two mappings - one of them is
    "ops" for binary operators, and other is "uns" for
    unary.
    To add your own operator do:

      1. Create function to do it (like "addv"/"negv")
         function foov(int256 v) private pure returns (int256) { return 42 + v; }
      2. Register it in constructor. Second argument is priority.
         It goes from bottom (1 is lowest priority) to top (255 is highest)
         Operators are one character in length!
         uns["#"] = uop ( foov, 3 );
      3. Add it to "isop()" function.
         function isop() ... { return ... || s == "#" || ... ; }
      4. It should work.

    Adding funcitions is same as adding operators (they are
    actually operators), except you skip step 3.
    Functions can take one argument and must return something.

    Named operators (like "xor") are added as functions, without
    third step.

  SYNTAX

    The basic operators are (sorted by priority):
      xor
      +, -
      *, /
      unary -
      function calls


    From functions, only abs() and sign() are supported

  TODO

     - Make more operators, more functions
     - Add multiple-argument functions


  SEE ALSO

    https://en.wikipedia.org/wiki/Shunting_yard_algorithm

*/

pragma ton-solidity >= 0.30.0;

contract calc {

  // Types

  struct hist_entry { string expr; int256 result; }

  struct op {
    // Usual operator
    function (int256, int256) view returns (int256) fn;
    uint8 prior;
  }

  struct uop {
    // Unary operator / function
    function (int256) view returns (int256) fn;
    uint8 prior;
  }

  enum optype { BIN, UNARY, PAREN }

  struct stackop {
    // Operator on stack
    optype t;
    string op;
    uint8 prior;
  }

  // Globals

  hist_entry[] public history;
  mapping(string => op) ops;
  mapping(string => uop) uns;

  // Functions & operators

  function addv(int256 a, int256 b) private pure returns (int256) { return a + b; }
  function subv(int256 a, int256 b) private pure returns (int256) { return a - b; }
  function mulv(int256 a, int256 b) private pure returns (int256) { return a * b; }
  function divv(int256 a, int256 b) private pure returns (int256) { return a / b; }
  function xorv(int256 a, int256 b) private pure returns (int256) { return a ^ b; }

  function negv(int256 a) private pure returns(int256) { return -a; }
  function absv(int256 a) private pure returns(int256) { return math.abs(a); }
  function signv(int256 a) private pure returns(int256) { return math.sign(a); }

  // Constructor which registers operators
  constructor() public {
    require(tvm.pubkey() != 0, 150);
    require(msg.pubkey() == tvm.pubkey(), 151);
    tvm.accept();
    ops["+"] = op ( addv, 1 );
    ops["-"] = op ( subv, 1 );
    ops["*"] = op ( mulv, 2 );
    ops["/"] = op ( divv, 2 );
    ops["xor"] = op ( xorv, 0 );

    uns["-"] = uop ( negv, 3 );
    uns["abs"] = uop ( absv, 4 );
    uns["sign"] = uop ( signv, 4 );
  }

  // Functions to check what type of character it is

  function isdigit(string s) private pure returns (bool) { return 48 <= uint8(bytes1(s)) && uint8(bytes1(s)) <= 57; }
  function isopch(string s) private pure returns (bool) {
    return s == "+" || s == "-" || s == "/" || s == "*" || s == "(" || s == ")";
  }

  // This function calculates stuff
  function calcStuff(string expr) public returns (int256) {
    tvm.accept();

    expr += " ";

    vector(int256) results;
    vector(stackop) stack;
    string last = "";
    bool isNum = true;
    bool shouldBeUnary = true;

    for (uint16 i = 0; i < expr.byteLength(); ++i) {
      string chr = expr.substr(i, 1);
      if (last != "" && (chr == " " || isdigit(chr) != isNum || isopch(chr) || isopch(last))) {
        // Flush something
        if (isNum) {
          // Number - on the result stack it goes
          results.push(stoi(last).get());
          shouldBeUnary = false;
        } else {
          // Operator
          if (last == "(") {
            // LParen is special operator
            stack.push(stackop ( optype.PAREN, "", 0 ));
          } else if (last == ")") {
            // RParen
            if (stack.length() == 0) revert(101, "Not opened parenthesis!");
            while (stack[stack.length()-1].t != optype.PAREN) {
              // Exec that op
              stackop s = stack.pop();
              if (s.t == optype.BIN) {
                int256 b = results.pop();
                int256 a = results.pop();
                results.push(ops[s.op].fn(a, b));
              } else if (s.t == optype.UNARY) {
                int256 v = results.pop();
                results.push(uns[s.op].fn(v));
              }
              if (stack.length() == 0) revert(101, "Not opened parenthesis!");
            }
            stack.pop();
          } else {
            // Usual operator
            stackop nop = stackop( optype.PAREN, last, 0 );
            if (shouldBeUnary && uns.exists(last))
              { nop.t = optype.UNARY; nop.prior = uns[last].prior; }
            else if (!shouldBeUnary && ops.exists(last))
              { nop.t = optype.BIN; nop.prior = ops[last].prior; }
            else
              revert(102, format("Unknown operator/function \"{}\"!", last));
            // Clean stack
            while (stack.length() != 0 && stack[stack.length()-1].t != optype.PAREN &&
              stack[stack.length()-1].prior >= nop.prior) {
              // Exec that op
              stackop s = stack.pop();
              if (s.t == optype.BIN) {
                int256 b = results.pop();
                int256 a = results.pop();
                results.push(ops[s.op].fn(a, b));
              } else if (s.t == optype.UNARY) {
                int256 v = results.pop();
                results.push(uns[s.op].fn(v));
              }
            }
            stack.push(nop);
          }
        }
        last = "";
      }
      if (chr != " ") {
        if (last.byteLength() == 0)
          isNum = isdigit(chr);
        last += chr;
      }
    }
    // Finish all operators
    while (stack.length() != 0) {
      require (stack[stack.length()-1].t != optype.PAREN, 104, "Unclosed parenthesis");
      stackop s = stack.pop();
      if (s.t == optype.BIN) {
        int256 b = results.pop();
        int256 a = results.pop();
        results.push(ops[s.op].fn(a, b));
      } else if (s.t == optype.UNARY) {
        int256 v = results.pop();
        results.push(uns[s.op].fn(v));
      }
    }
    // Grow history
    history.push(hist_entry(expr.substr(0, expr.byteLength()-1), results[0]));
    return results[0];
  }
}