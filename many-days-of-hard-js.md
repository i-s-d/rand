## Many days of hard JavaScript

This thing was inspired by 100daysofcss.
It was nice and fun, but it was easy / it had that sort of complexity what you would never need.
So, after some thoughts I made that list of random projects to make.
Those are hard ones (at least some of them), so doing one a day could be hard.
List is still in WIP form.

 1. Form maker
 2. Markdown
    Nothing unusual, just writting a markdown rendering engine.
 3. Syntax highlighting
    You've got some code, your task is to make it nice and readable.
    This challange is a start of series of code editor tasks
 4. Code editor
    While having some code is nice, editing it is way better
 5. Hints
    Inteljsense is nice!
    TODO: add icons
 6. Files & indexedDB
    What about multi-file projects and saving work?
    TODO: file icons
 7. Run HTML, JS, CSS!
    a-la Jsfiddle
 8. Markdown in editor
    Documentation is needed for good projects
    If you made code for "Markdown" challange universal, than this task is easy.
 9. Plots!
    Somewhat like Geogebra.
 10. Language (lisp?)
     Parsers are cool.
     Lazy people can make Lisp, while not lazy can consider using shunting algorithm
 11. Calendar
     Google calendar, anyone?
 13. Shell
     Fake linux in web browser
 14. Notion (sort of)
     Sortables arre handy here
 15. Nice todo list
     While everybody is making simple todo apps, there
     are almost not great todo apps.
 16. Spreadsheets
     Canvas or table?
 17. Watch object
     Start of framework challenge.
     You've got an object, your task is to find aut what properties change.
 18. Your own React
     Figure out Babel
 19. Vue
     Templates, properties...
 20. WebRTC based whiteboard
     Just copy-paste webrtc connection string.
     Or output qr-code.
 21. AsciiMath render
     Math is essential.
     No MathML, because it is easy. Use HTML & CSS!
 22. Object logger (easy)
     Nicely print JS Objects
 23. War with ADS?
 24. More war (Proxies, mutationObserver, ...)
 25. WebGl?
 26. Guess location
     Read theory about IP-s
 27. Cyberchef replica
     Extensible one!
 28. Graph app
 29. Worldgen
 30. Mercury Notebook
     Same as Jupyter, but easy one and working with JS.
 31. Sketcher
     Draw shapes, make constraints...
 32. Random SVG editor
     Or maybe Figma?
 33. Pixel art editor