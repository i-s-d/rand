{
  Калькулятор на Паскале, написанный без использования
  массивов, строк (не считая тех, что для write) и всего
  другого, сложнее uintNN, boolean и enum.

  Не требует никаких особых опций компиляции.
}
program calc;
{$MACRO on}

{ === Сканер с откатом до четырёх символов === }

var undone_n : uint8;
{ Стек отката, последний символ в нижнем разряде. }
var undone_chars : uint32;
{ Колонка }
var curr_col : uint8;

{
  Читаем символ
  Если мы возвращали что-то обратно, читаем вернутое.
}
function next() : char; begin
  if undone_n = 0 then read(next)
  else begin
    next := chr(undone_chars mod 256);
    undone_chars := undone_chars div 256;
    undone_n := undone_n - 1;
  end;
  curr_col := curr_col + 1;
end;

{
  Возвращаем один или несколько последовательных символов

  Больше четырёх возвращать нельзя, иначе "буффер"
  переполнится и возвращаемые пропадут. При попытке читать
  их мы получим нули.
}
procedure ungetc(c : uint32); begin
  while c <> 0 do begin
    undone_chars := undone_chars * 256 + c mod 256;
    c := c div 256;
    undone_n := undone_n + 1;
    curr_col := curr_col-1;
  end;
end;

{
  Подсматриваем следующие n символов
}
function peek(n : uint8) : uint32;
  var i : uint8;
begin
  { Просто читаем и откатываем }
  peek := 0;
  for i := 0 to n-1 do
    peek := peek + ord(next()) shl (i*8);
  ungetc(peek);
end;

{ === ctype === }

{ Является ли символ пробелом }
function isspace(ch : uint8) : boolean; begin
  case ch of
     { LF - не пробел }
    $20, { space }
    $0c, { FF}
    $0d, { CR }
    $09, { TAB }
    $0b:  { VTAB }
      isspace := true;
    else isspace := false;
  end;
end;

{ Является ли символ цифрой }
function isdigit(ch : uint8) : boolean; begin
  case ch of
    ord('0')..ord('9'): isdigit := true;
    else isdigit := false;
  end;
end;

{ Буквой? }
function isalpha(ch : uint8) : boolean; begin
  case ch of
    ord('a')..ord('z'),ord('A')..ord('Z'): isalpha := true;
    else isalpha := false;
  end;
end;

{ Символом, который встречается в знаках }
function issign(ch : uint8) : boolean; begin
  case ch of
    ord('+'), ord('-'),
    ord('*'), ord('/'),
    ord('^'):
      issign := true;
    else issign := false;
  end;
end;

{ === Чуть исключений на longjmp === }

{
  fail_reason особо не нужны, т.к. ошибки выводятся там же,
  где они кидаются.
}

type fail_reason = (
  TOO_LONG_FN_NAME,         { Имя функции длиннее четырёх символов }
  BRACKET_UNMATCHED,        { Скобки не хватило }
  BRACKET_EMPTY,            { Пустые скобки: "()" }
  MISSING_FN_ARG,           { Аргумента не хватает: "3 + cos" }
  UNKNOWN_UNARY,            { Префиксный оператор (или функция) неизвестен. }
  SIGN_SHOULDVE_BEEN_THERE, { Не хватает знака, вроде "2 2" }
  MISSING_OPERAND,          { Знака после префиксного оператора нет, вроде "(-)" }
  BAD_ARG,                  { Неправильный аргумент, вроде отрицательных чисел, }
                            { передаваемых корню }
  UNKNOWN_BINARY,           { Неизвестный бинарный оператор }
  EMPTY_EXPRESSION,         { Пользователь ничего не ввёл }
  UNKNOWN_FUNC              { Такой функции нет }
);

var fail_to : jmp_buf;
var fail_why : fail_reason;

procedure fail(reason : fail_reason); begin
  fail_why := reason;
  longjmp(fail_to, 1);
end;

{$define attempt:=if setjmp(fail_to)=0 then}
{$define catch:=else}


{ === Калькулятор === }

const PROMPT_LEN = 2;

{ Пропускаем пробелы из потока ввода }
procedure skipws(); begin
  while isspace(peek(1)) do next;
end;

{ Считаем длинну "буффера" из uint32 }
function buflen(buf : uint32) : uint8; begin
  if buf = 0 then buflen := 0
  else if buf < 256 then buflen := 1
  else if buf < 256*256 then buflen := 2
  else if buf < 256*256*256 then buflen := 3
  else buflen := 4;
end;

{ --- Функции для вывода ошибок --- }

{ Подчёркиваем ввод }
procedure underline(start : uint8; len : uint8);
var i : uint8;
begin
  for i := 1 to start+PROMPT_LEN do write(' ');
  write(chr($1b), '[31m');
  for i := start to start+len-1 do write('─');
  writeln();
  for i := 1 to start+PROMPT_LEN do write(' ');
  write('err: ', chr($1b), '[0m');
end;

{ Указываем на какой-то символ }
procedure point(at : uint8);
var i : uint8;
begin
  for i := 1 to at+PROMPT_LEN do write(' ');
  write(chr($1b), '[31m');
  writeln('▲');
  for i := 1 to at+PROMPT_LEN do write(' ');
  write('err: ', chr($1b), '[0m');
end;

{ --- Ввод токенов --- }

function read_num() : extended;
var p : extended;
begin
  skipws;
  read_num := 0;
  while isdigit(peek(1)) do
    read_num := read_num * 10 + (ord(next()) - ord('0'));
  if peek(1) = ord('.') then begin
    next;
    p := 0.1;
    while isdigit(peek(1)) do begin
      read_num := read_num + p * (ord(next()) - ord('0'));
      p := p * 0.1;
    end;
  end;
end;

function read_id() : uint32;
  var cnt, start : uint8;
begin
  cnt := 0;
  read_id := 0;
  start := curr_col;
  while isalpha(peek(1)) do begin
    read_id := read_id*256 + ord(next());
    cnt := cnt + 1;
  end;
  if cnt > 4 then begin
    underline(start, cnt);
    writeln('Too long name - max length is 4');
    fail(TOO_LONG_FN_NAME);
  end;
end;

{ --- Операторы --- }
{
  У бинарных и префиксных операторов обязаны быть
  разные приоритеты. Иначе рекурсивный алгоритм сломается.
  (можно было бы сделать так, чтобы он не ломался, но это
  больше работы)
}

const MAX_SIGN_LEVEL = 6;
const EPS = 0.00001; { Для проверки на целость }


function binary_level(s : uint32) : uint8;
begin
  case s of
    ord('+'),ord('-'): binary_level := 1;
    ord('*'),ord('/'),$6d6f64{mod}: binary_level := 2;
    { ... функции }
    ord('^'): binary_level := 5;
    else binary_level := 255;
  end;
end;

{ Выполняем оператор на a и b }
function exec_binary(name : uint32; at : uint8; a : extended; b : extended) : extended;
var rev : boolean;
begin
  case name of
    ord('+'): exec_binary := a + b;
    ord('-'): exec_binary := a - b;
    ord('*'): exec_binary := a * b;
    ord('/'): begin
      if abs(b) < EPS then begin
        point(at);
        writeln('Division by zero');
        fail(BAD_ARG);
      end;
      exec_binary := a / b;
    end;
    $6d6f64: begin { mod }
      if (abs(trunc(a)-a) > EPS) or (abs(trunc(b)-b) > EPS) then begin
        underline(at, 3);
        writeln('`mod` operator is defined only for integers');
        fail(BAD_ARG);
      end;
      exec_binary := trunc(a) mod trunc(b);
    end;
    ord('^'): begin { Возведение в степень }
      if a < 0 then begin
        if abs(trunc(b)-b) < EPS then begin
          a := -a;
          rev := (trunc(b) mod 2) = 1;
        end else begin
          point(at);
          writeln('Cannot calculate ', a:4:2, '^', b:4:2, ' - base < 0, power is not integer');
          fail(BAD_ARG);
        end;
      end;
      exec_binary := exp(b * ln(a));
      if rev then exec_binary := -exec_binary;
    end;
    else begin
      underline(at, buflen(name));
      writeln('Unknown operator');
      fail(UNKNOWN_BINARY);
    end;
  end;
end;

function unary_level(name : uint32) : uint8;
begin
  case name of
    ord('-'): unary_level := 4;
    else unary_level := 3;
  end;
end;

{ Выполняем функцию или префиксный оператор. }
function exec_unary(name : uint32; at : uint8; arg : extended) : extended;
begin
  case name of
     ord('-'): exec_unary := -arg;
      $73696e: exec_unary := sin(arg);
      $636f73: exec_unary := cos(arg);
    $73717274: exec_unary := sqrt(arg);
      $616273: exec_unary := abs(arg);
    $6174616e: exec_unary := arctan(arg); { atan }
      $657870: exec_unary := exp(arg);
        $6c6e: begin { ln }
      if arg <= 0 then begin
        underline(at, 2);
        writeln('Natural logarithm for negative numbers is not defined');
        fail(BAD_ARG);
      end;
      exec_unary := ln(arg);
    end;
    else begin
      underline(at, buflen(name));
      writeln('Unknown operator');
      fail(UNKNOWN_UNARY);
    end;
  end;
end;

procedure assert_func_exists(name : uint32; at : uint8);
begin
  case name of
    $73696e, $636f73, $73717274,
    $616273, $6174616e, $6c6e, $657870: ;
    else begin
      underline(at, buflen(name));
      writeln('Unknown function');
      fail(UNKNOWN_FUNC);
    end;
  end;
end;

function lookup_const(name : uint32; var out : extended) :  boolean;
begin
  lookup_const := true;
  case name of
    $7069: out := 3.141592653589793; { pi }
    $65: out := 2.718281828459045; { e }
    else lookup_const := false;
  end;
end;

{
  Само выполнение выражений.
  Читает подвыражение уровня LEVEL, например если
  на вводе будет 1 * 2 / 3 + 4 и LEVEL = 2 (соответствует умножению)
  будет прочтено "1 * 2 / 3", выполнено и вернуто.
}
function term (
  level : uint8;
  missing_fail_reason : fail_reason) : extended;
var buf : uint32;
var fn_begin : uint8;
begin
  skipws;

  buf := 0;

  { Читаем первый элемент }

  if issign(peek(1)) then begin { Префиксные операторы }
    fn_begin := curr_col;
    buf := ord(next());
    if unary_level(buf) = 255 then begin
      underline(fn_begin, 1);
      writeln('Unknown operator');
      fail(UNKNOWN_UNARY);
    end else if unary_level(buf) <> level then begin
      ungetc(buf);
      term := term(level+1, missing_fail_reason);
    end else
      term := exec_unary(buf, fn_begin, term(unary_level(buf), MISSING_OPERAND));
  end else if isalpha(peek(1)) then begin { Константы/функции }
    fn_begin := curr_col;
    buf := read_id();

    if lookup_const(buf, term) then begin
      if level <> MAX_SIGN_LEVEL+1 then begin
        ungetc(buf);
        term := term(level+1, missing_fail_reason);
      end;
    end else begin
      assert_func_exists(buf, fn_begin);
      term := exec_unary(buf, fn_begin, term(unary_level(buf), MISSING_FN_ARG));
    end;
  end else if level = MAX_SIGN_LEVEL+1 then begin
    if isdigit(peek(1)) then begin { Числа }
      term := read_num();
    end else if peek(1) = ord('(') then begin { Скобки }
      fn_begin := curr_col;
      next; { "(" }
      term := term(1, BRACKET_EMPTY);
      if peek(1) <> ord(')') then begin
        point(fn_begin);
        writeln('Missing matching bracket');
        fail(BRACKET_UNMATCHED);
      end;
      next; { ")" }
    end else begin
      if missing_fail_reason = MISSING_OPERAND
        then begin point(curr_col); writeln('Missing operand'); end
      else if missing_fail_reason = MISSING_FN_ARG
        then begin point(curr_col); writeln('Missing function argument'); end
      else if missing_fail_reason = EMPTY_EXPRESSION
        then begin point(curr_col); writeln('Expression is empty'); end
      else if missing_fail_reason = BRACKET_EMPTY
        then begin underline(curr_col-1,2); writeln('Empty brackets'); end
      else
        writeln(missing_fail_reason);
      fail(missing_fail_reason);
    end
  end else {  }
    term := term(level+1, missing_fail_reason);
  skipws;

  { Читаем остальную часть. }
  { Например: level = 1, читаем <term: 3.14> + <term: sin 6> - <term: 4> + <term: 2*3> }
  while (peek(1) <> ord(')')) and (peek(1) <> $0a { NL }) do begin
    fn_begin := curr_col;
    if issign(peek(1))
      then buf := ord(next())
    else if isalpha(peek(1))
      then buf := read_id();
    if isdigit(buf) or lookup_const(buf, term) then begin
      point(fn_begin-1);
      writeln('Sign expected between values');
      fail(SIGN_SHOULDVE_BEEN_THERE);
    end;

    if binary_level(buf) < level then begin { Читали 2 * 3 * 4, прочитали + }
      ungetc(buf);
      break;
    end else if binary_level(buf) = 255 then begin
      underline(fn_begin, buflen(buf));
      writeln('Unknown operator');
      fail(UNKNOWN_BINARY);
    end;
    skipws;
    term := exec_binary(buf, fn_begin, term, term(binary_level(buf)+1, MISSING_OPERAND));
    skipws;
  end;
end;

var res : extended;

begin

  writeln('Welcome to calculator, written without any arrays or strings');
  writeln('All usual pascal operations are supported');
  writeln(chr($1b), '[2mPress Ctrl-C to exit', chr($1b), '[0m');
  while true do begin
    curr_col := 0;
    undone_n := 0;
    undone_chars := 0;

    write(chr($1b),'[34m> ',chr($1b), '[0m');
    attempt begin
      res := term(1, EMPTY_EXPRESSION);
      writeln(chr($1b), '[33m', res:5:6, chr($1b), '[0m');
    end catch begin
      { writeln('Exception ', fail_why); }
    end;
    while peek(1) <> $0a do next;
    next;
  end;
end.