/*
	-------------------------------------------------------------------
	To any non-russian person, who ventured here - this is a
	tiny homemade libc, done for fun and to solve some tasks in school.
	-------------------------------------------------------------------
*/

//
// Данный файл представляет из себя игрушечную libc, написанную
// для Linux x86_64. Изначальное (и единственное) предназначение
// этого является написание таких задач, которые дают в базовом
// курсе по программированию.
//
// Много чего не реализовано, соответствия стандартам нет.
//
// Чего есть:
//   - syscall и несколько обёрток для ввода-вывода (в т.ч файлового)
//   - Быстрое возведение в степень, потому что было нужно
//   - Ввод - getint, getstr, getc, skipws. Без scanf и FILE*, всё на дескрипторах.
//   - int unget = char или -1. Простенький вариант ungetc().
//   - Вывод - print() для строк и чисел, до 11 аргументов.
//   - strlen(), isspace()
//

#include <stdint.h> // intXX_t
#include <stdbool.h>

// === Функции из ассемблера ===
extern int64_t _nsyscall(int call, ...);
extern int64_t _syscall6(int call, ...);

// === Красивая обёртка syscall ===
// Добываем восьмой из параметров
#define _eighth(a,b,c,d,e,f,g,h,...) h
// Используя прошлый макрос, возвращаем _syscall6 если аргументов шесть, иначе _nsyscall
#define _chose_syscall(...) _eighth(0,__VA_ARGS__,\
	_syscall6,_nsyscall,_nsyscall,_nsyscall,_nsyscall,_nsyscall, _nsyscall)
// Макрос-обёртка
#define syscall(n, ...) _chose_syscall(__VA_ARGS__)(n, ##__VA_ARGS__)
// Теперь можно делать syscall() и не беспокоиться о количестве аргументов

// === Обёртки к разным вызовам syscall-а ===

// Завершаем программу
void __attribute__((noreturn)) exit(int rc) {
	syscall(0x3c, rc);
	__builtin_unreachable();
}

// Выводим что-то
int write(int fd, void* bytes, int len) {
	return syscall(0x01, fd, bytes, len);
}

// Вводим что-то, возвращает количество введённого.
int read(int fd, void* buf, int maxlen) {
	return syscall(0x00, fd, buf, maxlen);
}

// Открываем файл
// TODO: mode, создание файлов и т.д.
int open(const char* filename, int flags) {
	return syscall(0x02, filename, flags, 0);
}

// Флаги для open, не все.
enum file_flags {
	O_RDONLY=0,
	O_WRONLY=1,
	O_RDWR=2
};

enum seek_whence_t {
	SEEK_SET = 0,
	SEEK_CUR = 1,
	SEEK_END = 2
};

// Обычный fseek, только для дескрипторов.
// Возвращает итоговое положение.
int lseek(int fd, int offset, enum seek_whence_t whence) {
	return syscall(0x08, fd, offset, whence);
}

// Обрезаем файл по какую-то длинну
int ftruncate(int fd, unsigned len) {
	return syscall(0x4d, fd, len);
}

// Закрываем файл
int close(int fd) {
	return syscall(0x03, fd);
}

// === Обработка переполнения стека ===

const char __stack_chk_fail_msg[] = "Stack smashing detected!\n";

void __stack_chk_fail() {
	write(3, (void*) __stack_chk_fail_msg, sizeof(__stack_chk_fail_msg)-1);
	exit(-1);
}

// === Математика ===
// Ничего с плавающей точкой нет, т.к. я не игру пишу.

// Возведение в степень
int64_t pow(int64_t a, int64_t pow) {
	int64_t rs = 1;
	while (pow) {
	if (pow & 1) rs *= a;
	a *= a; pow >>= 1;
	}
	return rs;
}

// === Ввод ===

#define EOF -1

// Символ, который мы "вернули" обратно на ввод
int unget = -1;

// Читаем один символ, если получается
// Если мы что-то возвращали обратно,
// мы это получаем.
int getc() {
	if (unget != -1) {
	int t = unget;
	unget = -1;
	return t;
	}
	char c;
	if (!read(0, &c, 1))
	return EOF;
	return c;
}

// Определяем, является ли символ пробелом.
bool isspace(int c) {
	return c == ' ' || c == '\t' || c == '\n' || c == '\r';
}

// Пропускаем пробелы
void skipws() {
	int c;
	while (isspace(c = getc()));
	unget = c;
}

// Читаем целое число
int getint() {
	skipws();
	char c = getc();
	bool inv = false;
	if (c == '-') inv = true, c = getc();
	int n = 0;
	while ('0' <= c && c <= '9') {
	n = (n*10) + (c - '0');
	c = getc();
	}
	if (inv) n = -n;
	return n;
}

// Читаем строку до пробела или до длинны maxlen
void getstr(char* buf, int maxlen) {
	skipws();
	int c;
	while (!isspace(c = getc()) && c != EOF && maxlen--)
	*(buf++) = c;
	*buf = 0;
	unget = c;
}

// === Строки ===

// Вычисляем длинну строки
int strlen(char* s) {
	int ln = 0;
	while (s[ln]) ++ln;
	return ln;
}

// Переводим один символ в СС
int ctoi(char c) {
	if (c >= '0' && c <= '9')
	return c - '0';
	else if (c >= 'a' && c <= 'z')
	return c - 'a' + 10;
	else if (c >= 'A' && c <= 'Z')
	return c - 'A' + 10;
	return 0;
}

// Переводим строку в число
// Основание до 36
int64_t stoi(char* str, int base) {
	// Подобное было в getint()
	int64_t rs = 0;
	bool inv = false;
	if (*str == '-') inv = true, ++str;
	while (*str)
	rs = rs * base + ctoi(*(str++));
	return inv ? -rs : rs;
}

// === Вывод ===

// Выводим число
void _printint(int64_t v) {
	char res[22];
	if (v == 0) { res[0] = '0'; write(1, res, 1); }
	if (v < 0) {
	res[0] = '-'; write(1, res, 1);
	v = -v;
	}
	int ln = 0;
	while (v && ln < 21) {
	res[21-ln] = (v % 10) + '0';
	v /= 10;
	++ln;
	}
	write(1, res+22-ln, ln);
}

// Выводим строку
void _printstr(char* str) {
	write(1, str, strlen(str));
}

// Выводим одно значение
#define _print_one(val) _Generic((val), \
		char*: _printstr,\
		int: _printint,\
		int64_t: _printint\
	)(val)

// Получаем первый аргумент
#define fst(a, ...) a
// Получаем все, кроме первого
#define nfst(a, ...) __VA_ARGS__
// Делаем трюк, чтобы сделать питоноподобный print
// Как видно, данный вариант печатает до пяти аргументов
#define _maybe_print1(...) __VA_OPT__(_print_one(fst(__VA_ARGS__));)
#define _maybe_print2(...) __VA_OPT__(_print_one(fst(__VA_ARGS__));_maybe_print1(nfst(__VA_ARGS__)))
#define _maybe_print3(...) __VA_OPT__(_print_one(fst(__VA_ARGS__));_maybe_print2(nfst(__VA_ARGS__)))
#define _maybe_print4(...) __VA_OPT__(_print_one(fst(__VA_ARGS__));_maybe_print3(nfst(__VA_ARGS__)))
#define _maybe_print5(...) __VA_OPT__(_print_one(fst(__VA_ARGS__));_maybe_print4(nfst(__VA_ARGS__)))
#define _maybe_print6(...) __VA_OPT__(_print_one(fst(__VA_ARGS__));_maybe_print5(nfst(__VA_ARGS__)))
#define _maybe_print7(...) __VA_OPT__(_print_one(fst(__VA_ARGS__));_maybe_print6(nfst(__VA_ARGS__)))
#define _maybe_print8(...) __VA_OPT__(_print_one(fst(__VA_ARGS__));_maybe_print7(nfst(__VA_ARGS__)))
#define _maybe_print9(...) __VA_OPT__(_print_one(fst(__VA_ARGS__));_maybe_print8(nfst(__VA_ARGS__)))
#define _maybe_print10(...) __VA_OPT__(_print_one(fst(__VA_ARGS__));_maybe_print9(nfst(__VA_ARGS__)))
#define _maybe_print11(...) __VA_OPT__(_print_one(fst(__VA_ARGS__));_maybe_print10(nfst(__VA_ARGS__)))
#define print(...) do { _maybe_print11(__VA_ARGS__) } while (0)

// TODO: много чего